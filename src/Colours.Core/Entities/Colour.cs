﻿
namespace Colours.Core.Entites
{
    public class Colour
    {
        public int ColourId { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
    }
}
