﻿using Colours.Core.Entities;
using System.Collections.Generic;

namespace Colours.Core.Interfaces
{
    interface IPersonRepository
    {
        IEnumerable<Person> GetPeople();
        Person GetPerson(int PersonId);

        Person UpdatePerson(Person person);
    }
}
