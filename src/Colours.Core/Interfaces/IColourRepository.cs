﻿using Colours.Core.Entites;
using System.Collections.Generic;

namespace Colours.Core.Interfaces
{
    interface IColourRepository
    {
        IEnumerable<Colour> GetColours();
        Colour GetColour(int ColourId);
    }
}
